package tc.greta.formation.manvoyage;

import java.io.FileWriter;
import java.util.Scanner;

public class Afrique {

	public static void faireNamibie() throws RuntimeException {
		Scanner sc = new Scanner((System.in));
		try {
			System.out.println("nom");
			String nom = sc.next();
			if (nom.equals("Thanos"))
				throw new IllegalArgumentException("Avengers assemble !");
			System.out.println("email");
			String email = sc.next();
			if (!email.substring(2, email.length() - 2).contains("@"))
				System.out.println("Erreur email");
			FileWriter fw = new FileWriter("namibie.csv", true);// création d'un fichier csv
			fw.write(nom + "," + email + "\n");
			fw.close();
		} catch (IllegalArgumentException ex) {
			System.out.println("Thanos vaincu !" + ex);
		} catch (Exception ex) {
			System.out.println("C'est dommage" + ex);
		} finally {
			sc.close();
		}
	}

	public static void faireMadagascar() {
		Scanner sc = new Scanner((System.in));
		try {
			System.out.println("nom");
			String nom = sc.next();
			System.out.println("email");
			String email = sc.next();
			System.out.println("nb");
			int nb = sc.nextInt();

			AfriqueDAO dao = new AfriqueDAO();
			dao.insereDemande(nom, email, nb);
			dao.afficheParEmail(email);
			dao.fermer();

		} catch (Exception ex) {
			System.out.println(ex);
		} finally {
			sc.close();
		}
	}
}
