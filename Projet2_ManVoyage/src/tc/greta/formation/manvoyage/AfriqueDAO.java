package tc.greta.formation.manvoyage;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class AfriqueDAO {

	private Connection conn;

	public AfriqueDAO() throws SQLException {
		this.conn = DriverManager.getConnection("jdbc:sqlite:madagascar.sqlite");
		Statement st = conn.createStatement();
		st.executeUpdate(
				"CREATE TABLE IF NOT EXISTS demande(id INTEGER PRIMARY KEY, nom TEXT, email TEXT, " + "nb INT)");

	}

	public void insereDemande(String nom, String email, int nb) throws SQLException {
		PreparedStatement ps = conn.prepareStatement("INSERT INTO demande(nom, email, nb)VALUES(?,?,?)");
		ps.setString(1, nom);// Voilà une requête préparée !
		ps.setString(2, email);
		ps.setInt(3, nb);
		int n = ps.executeUpdate();
		System.out.println(String.format("Demande %d", n));
	}

	public void afficheParEmail(String email) throws SQLException {
		PreparedStatement ps2 = conn.prepareStatement("SELECT * FROM demande WHERE email =? ORDER BY nom");
		ps2.setString(1, email);
		ResultSet rs = ps2.executeQuery(); // ResultSet est un statement, fonction next
		while (rs.next()) {
			System.out.print(rs.getString("nom"));
			System.out.print(rs.getString("email"));
			System.out.println(rs.getString("nb"));

		}
		rs.close();
	}

	public void fermer() throws SQLException {
		conn.close();
	}

}
