package tc.greta.formation.manvoyage;

public class Amerique {

	public static double calculerTotal(float hotel, float avion, double promo, int jours) {
		float total1 = jours * hotel;
		float total2 = total1 + avion;
		double total3 = total2 - total2 * promo / 100;
		return total3;
	}

	public static double calculerTotal2(float hotel, float avion, int jours) {
		double total2 = calculerTotal(hotel, avion, 0, jours);
		return total2;
	}

	public static void faireBresil() {
		System.out.println("Welcome to brazil");
		int dest = 3;
		System.out.println(calculerBresil(dest) + " trajets");
	}

	public static void faireCanada(float hotel, float avion) {

		java.util.Scanner sc = new java.util.Scanner(System.in);
		int jours = (int) sc.nextInt();

		if (jours < 0)
			System.err.println("Erreur fatale");
		else if (jours > 150)
			System.out.println("Pas ça zinédine");
		else if (jours > 75)
			System.out.println("Bon voyage !");

		float promo = 20;
		// int jours = 7;
		// float hotel = 48;
		// float avion = 860;

		// float total1 = jours * hotel;
		// float total2 = total1 + avion;
		double total3 = Amerique.calculerTotal(hotel, avion, promo, jours);

		System.out.println(String.format("Voyage à Zegema Beach pour seulement %.2f \u20ac", total3));

		int semaine = jours / 7;
		switch (semaine) {
		case 0:
		case 1:
			System.out.println("pas de cadeau é_è");
			break;
		case 2:
			System.out.println("Balade en aquaponey cadeau");
			break;
		case 3:
		case 4:
			System.out.println("sucette au brocoli");
			break;
		case 5:
			System.out.println("1 match de hockey avec Idris elba");
			break;
		default:
			System.out.println("choqué et déçu");
		}

		int[] joursSpec = { 5, 11, 13, 22 };
		boolean[][] tb = new boolean[2][4];
		tb[0][2] = true;
		// byte[][][] pi = new byte[640][480][3];
		// = pi.length;
		// = pi[0].length;

		for (int i = 1; i <= jours; i = i + 1) {

			if (i > 1) {
				System.out.println("Jour de ski");
			} else if (i % 5 == 0) {
				System.out.println("Jour de poutine");
			} else {
				System.out.println("Jour d'avion");
			}
			// for (int js = 0; js < joursSpec.length; js = js + 1)
			// if (joursSpec[js] == i)
			// System.out.println("Jour spectacle");
			for (int js : joursSpec)
				if (js == i)
					System.out.println("Jour spectacle");
		}

		// double[] td = new double[6];
		// = new double[] {0,0,0,0};
		// td[5] = -1.1;

		sc.close();

	}

	static int calculerBresil(int d) {
		if (d == 1)
			return 1;
		else
			return d * calculerBresil(d - 1);
	}
}
