package tc.greta.formation.manvoyage;

//Faire appel à une classe nécessite 1) package commun, 2) appartient à java.lang(package standart), 3) préfixer et 4) importer (commande import, d'un package)
public class Debut {

	public static void main(String[] args) {
		System.out.println("Tu voulais voir la galaxie ?");
		// Amerique.faireCanada(48, 680);
		// Amerique.faireBresil();
		// Europe.faireEspagne();
		// Europe.faireItalie();
		// Europe.faireRussie();
		// Europe.faireTurquie();
		// Afrique.faireNamibie();
		// Afrique.faireMadagascar();
		 Asie.faireJapon();
		
	}

}

// Je suis un commentaire >_<
//Ctrl shift F pour ranger le code