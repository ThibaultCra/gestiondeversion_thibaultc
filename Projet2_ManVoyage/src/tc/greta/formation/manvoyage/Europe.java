package tc.greta.formation.manvoyage;//Toujours définir un package 

import java.time.LocalDateTime;
import java.util.*;
import java.util.Scanner;

import tc.greta.formation.manvoyage.donnees.Destination;
import tc.greta.formation.manvoyage.donnees.DestinationMaritime;
import tc.greta.formation.manvoyage.donnees.Nomable;

public class Europe {

	public static void faireEspagne() {

		Destination d1 = new Destination();
		d1.setNom("Andalousie");
		d1.setPays(" Espagne ");
		d1.setJours(10);

		d1.allonger(10);
		System.out.println(d1.toString());

		System.out.println(d1.toString());
		Destination d2 = d1;// copie par référence
		System.out.println(d2.toString());
		System.out.println(d1 == d2);
		d2.setNom("Saragoza");
		System.out.println(d1.toString());// modifier d2 modifie d1, c'est le même espace mémoire

		Destination d3 = new Destination("madrid", 20, "espagne");
		// d3.nom = "madrid";
		// d3.pays = "Espagne";
		// d3.jours = 20;
		System.out.println(d1.equals(d3));

	}

	public static void faireItalie() {
		System.out.println("Ma que pizza !");

		DestinationMaritime i1 = new DestinationMaritime();
		i1.setNom("Syracuse");
		i1.setPays(" Italie ");
		i1.setJours(4);
		i1.setIle(" Sardaigne");
		System.out.println(i1.toString());
		final Nomable i2;// Final est utile pour faire des constantes globales
		i2 = new DestinationMaritime();
		i2.setNom("Catane");
	}

	public static void faireRussie() {
		System.out.println("Prostagma !");
		Scanner sc = new Scanner((System.in));
		System.out.println("lieu");
		String lieu = sc.next();
		System.out.println("nom");
		String nom = sc.next();
		System.out.println("telephone");
		String telephone = sc.next();

		LocalDateTime departRussie = null;// Calculer la date de départ en prenant en compte l'administratif
		LocalDateTime prepRussie = LocalDateTime.now();
		departRussie = prepRussie.plusDays(5).plusWeeks(4);
		System.out.println(
				String.format("depart le %02d/%02d", departRussie.getDayOfMonth(), departRussie.getMonthValue()));

		lieu = lieu.trim();// Redéfinition du lieu
		lieu = lieu.substring(0, 1).toUpperCase() + lieu.substring(1);

		// formatage des Strings : s = String.format("abc %d %%, "i");

		System.out.println(lieu);

	}

	public static void faireTurquie() {
		Scanner sc = new Scanner((System.in));
		// String Etape = sc.next();
		// do {System.out.println("Etape");} while(Etape != "exit");

		// String t1 = "Ankara";
		// String t2 = "Istanbul";
		// String t3 = "Izmir";
		String e;
		int n = 1;
		double tot = 0;
		Map<String, Double> suppls = new HashMap<String, Double>();
		suppls.put("Trebizonde", 349.9);
		List<String> l;// Etablissement de la collection avec variable "l"
		l = new ArrayList<String>();
		do {
			System.out.println("Etape" + n);
			e = sc.next();
			l.add(e);
			if (suppls.get(e) != null)
				tot += suppls.get(e);
			n++;
		} while (!e.equals("exit"));// Toujours utiliser equals pour comparer des chaînes
		sc.close();
		for (String et : l)// Deuxième boucle pour la nouvelle variable de la collection
			System.out.println(et);
		System.out.println(tot);

		// Map<String,Integer> supp = new HashMap<String,Integer>();
		// supp.put(t1,200);
		// supp.put(t2,100);
		// supp.put(t3,150);
		// System.out.println(supp);

		// List<String> e2;
		// e2 = new ArrayList<String>();
		// e2.add(e + n);
		// System.out.println(e2.get(0));

	}

}
