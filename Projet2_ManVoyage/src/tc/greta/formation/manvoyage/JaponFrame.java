package tc.greta.formation.manvoyage;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;

public class JaponFrame extends JFrame implements MouseListener, ActionListener {

	private Color bg;
	private JTextField tf1;
	private JLabel lb3;

	public JaponFrame() {
		super("Japon");
		this.setSize(200, 400);// Abscisse puis ordonnée en partant de en haut à gauche
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);// Fermer la fenêtre fait sortir de l'appli
		getContentPane().setLayout(null);

		JButton bt = new JButton("Enregistrer");
		bt.addActionListener(this);
		getContentPane().add(bt);
		bt.setBounds(20, 250, 150, 50);// Coordonnées du bouton Abscisse, ordonnée, largeur, hauteur

		JLabel lb = new JLabel("JAPON");
		getContentPane().add(lb);
		lb.setBounds(70, 10, 200, 50);
		JLabel lb2 = new JLabel("Nouveau voyage");
		getContentPane().add(lb2);
		lb2.setBounds(50, 50, 200, 30);
		lb3 = new JLabel("Veuillez tout remplir");
		getContentPane().add(lb3);
		lb3.setBounds(40, 300, 150, 50);
		JLabel lb4 = new JLabel("Nom");
		getContentPane().add(lb4);
		lb4.setBounds(15, 90, 200, 50);
		JLabel lb5 = new JLabel("Tel");
		getContentPane().add(lb5);
		lb5.setBounds(15, 150, 200, 30);
		JLabel lb6 = new JLabel("Jours");
		getContentPane().add(lb6);
		lb6.setBounds(15, 190, 200, 50);

		JSpinner js = new JSpinner(new SpinnerNumberModel(5, 2, 30, 1));
		getContentPane().add(js);
		js.setBounds(60, 200, 100, 30);

		tf1 = new JTextField("");
		// tf1.setBackground(Color.WHITE);
		tf1.addMouseListener(this);
		getContentPane().add(tf1);
		tf1.setBounds(60, 100, 100, 30);
		JTextField tf2 = new JTextField("");
		tf2.addMouseListener(this);
		// tf2.setBackground(Color.WHITE);
		getContentPane().add(tf2);
		tf2.setBounds(60, 150, 100, 30);
		// JTextField tf3 = new JTextField("");
		// getContentPane().add(tf3);
		// tf3.setBounds(60, 200, 100, 30);

		bg = tf1.getBackground();

		this.setVisible(true);

	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent ne) {
		// TODO Auto-generated method stub
		System.out.println("wololo");
		Component c = ne.getComponent();
		c.setBackground(Color.YELLOW);

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		Component c2 = e.getComponent();
		c2.setBackground(bg);
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent re) {
		// TODO Auto-generated method stub

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		System.out.println("Nuclear launch initiated");
		
		lb3.setText(">>>" + " " + tf1.getText() + " " + "<<<");
		}

}
