package tc.greta.formation.manvoyage.donnees;

//Un bloc de javadoc :
/**
 * Classe de caracterisation des differentes destinations
 * 
 * @author Java
 * @since 12/03/2019
 */
public class Destination implements Nomable {

//Une classe peut implémenter un nombre quelconque d'interfaces
//En java, il est possible de ne rien indiquer à public/pivate/protected
//En général, les attributs sont privés et les méthodes sont publiques
	private String nom;// Attributs
	private String pays;
	private int jours;

	public Destination(Destination d) {// constructeur de clonage
		this(d.nom, d.jours, d.pays);
	}
/**
 * Voici les parametres !
 * @param n nom de la destination
 * @param j nombre de jours de voyage
 * @param p pays de reve
 */
	public Destination(String n, int j, String p) {// Constructeur(au nom de la classe)
		this.nom = n;
		this.jours = j;
		this.pays = p;
	}

	public Destination() {
		this("cancun", 0, "espagne");
	}

	public boolean equals(Destination d) {// Exemple de méthode Booléen
		if (d.nom == (this.nom))
			return true;
		return false;
	}

	@Override // Annotation, indication pour le compilateur, vérifie que c'est bien écrit
	public String toString() {
		return nom + pays + jours;
	}

	public void allonger(int j) {// méthode
		jours = j + jours;
	}

	public void finalize() {// Destructeur
		System.out.println("End");
	}

	public String getNom() {// Encapsulation (clic droit => source => Getter setter)
		return nom;
	}

	public String getPays() {
		return pays;
	}

	public int getJours() {
		return jours;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public void setPays(String pays) {
		this.pays = pays;
	}

	public void setJours(int jours) {
		this.jours = jours;
	}

}
