package tc.greta.formation.manvoyage.donnees;
//Classe héritière de destination !
public class DestinationMaritime extends Destination {

	private String ile;// Définition du nouvel attribut
	private final static String ILE_DEFAULT = null; // Constante, le final est important dans sa définition

	public String toString() {// Cette fonction fait appel au toString de la classe mère Destination
		return super.toString() + ile;// il s'agit d'une redéfinition
	}

	public String getIle() {// Encapsulation[Getter]
		return ile;
	}

	public void setIle(String ile) {// Encapsulation[Setter]
		this.ile = ile;
	}

	public DestinationMaritime() {// Constructeur
		super();
		ile = ILE_DEFAULT;
	}

	public DestinationMaritime(String n, int j, String p, String i) {// Constructeur qui fait appel aux attributs de
																		// Destination
		super(n, j, p);
		this.ile = i;// Le this est optionnel
	}

	// public abstract void rien(); ===> exemple de classe abstraite, il faut
	// ajouter abstract dans le nom de la classe et new devient interdit
}
