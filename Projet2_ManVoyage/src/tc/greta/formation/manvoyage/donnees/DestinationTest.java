package tc.greta.formation.manvoyage.donnees;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class DestinationTest {

	private Destination d;

	@BeforeEach
	void before() {
		d = new Destination();
	}

	@Test
	void testCtor() {
		assertEquals(0, d.getJours());
		// assertEquals(...,d.getNom());
	}

	@Test
	void testSetPays() {
		d.setPays("espagne");
		assertEquals("espagne", d.getPays());
	}

	@Test
	void testAllonger() {
		d.allonger(0);
		assertEquals(0, d.getJours());
		// d.allonger(0);
	}

}
