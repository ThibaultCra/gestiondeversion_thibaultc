package projet4_avions;

import javax.persistence.Column;
//Chaque annotation est une classe d'ou la présence d'imports.
import javax.persistence.Entity;
import org.springframework.data.annotation.Id;
import org.springframework.data.gemfire.mapping.annotation.Region;

@Region("balloonRegion")//Lien au data.xml
@Entity
public class Balloon {
	
	private Integer id;
	private String name;
	private double size;
	private double altmax;
	
	@Id
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@Column
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getSize() {
		return size;
	}
	public void setSize(double size) {
		this.size = size;
	}
	public double getAltmax() {
		return altmax;
	}
	public void setAltmax(double altmax) {
		this.altmax = altmax;
	}
	
	public Balloon(int id, String name, double size, double altmax) {
		this.id = id;
		this.name = name;
		this.size = size;
		this.altmax = altmax;	
	}
		
	public Balloon() {
		this(0, "Balloon", 0, 0);
	}
	
	public String toString() {
		return "Balloon:" + " " + "id=" + " " + id + " ; " + "name=" + " " + name + " ; " 
				+ "size=" + " " + size + " ; " + "altmax=" + " " + altmax;
	}
}
