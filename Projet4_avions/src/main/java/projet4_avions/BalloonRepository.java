package projet4_avions;


import org.springframework.data.repository.CrudRepository;

//Le DAO du NoSQL. Un type de donnée pour un repository.
public interface BalloonRepository extends CrudRepository<Balloon, Integer>{

	Iterable<Balloon> findByName(String n); 
	Iterable<Balloon> findBySizeGreaterThanOrderByName(double s);
	Iterable<Balloon> findByAltmaxGreaterThanAndAltmaxLessThanOrderBySize(double min, double max);
	
}