package projet4_avions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.gemfire.repository.config.EnableGemfireRepositories;
import org.springframework.stereotype.Service;

@EnableGemfireRepositories("projet4_avions")
@Service("balloonService")
public class BalloonService {
	
	private BalloonRepository repo;
	
	public void execute() {
		System.out.println("Bring more balloons !");
		//System.out.println(repo);  Est une ligne de test !
		repo.save(new Balloon(1, "La Salsa", 12, 2300));
		repo.save(new Balloon(2, "La Tequila", 4, 1200));
		repo.save(new Balloon(3, "El Corrazon", 15, 19000));
		repo.save(new Balloon(4, "La Cerveza", 1, 6700));
		repo.save(new Balloon(5, "El muy Bueno", 100, 256000));
		/**
		for(Balloon b1:repo.findAll()) {
			b1.setAltmax(b1.getAltmax()/3200);
		}
		
		for(Balloon b2:repo.findAll()) {
			System.out.println(b2);
		}
		
		for(Balloon b3:repo.findBySizeGreaterThanOrderByName(8)) {
			System.out.println(b3);
		}**/
		
		for(Balloon b4:repo.findByAltmaxGreaterThanAndAltmaxLessThanOrderBySize(5000, 20000)) {
			System.out.println(b4);
		}	
		
	}
	@Autowired
	public void setRepo(BalloonRepository repo) {
		this.repo = repo;
	}
}
