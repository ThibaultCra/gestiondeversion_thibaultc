package projet4_avions;

import java.time.LocalDateTime;
import java.util.Set;

public abstract class Carrier {

	private String name;
	private LocalDateTime foundation;
	private Coordinate office;
	private Set<Plane> planes;

	public Coordinate getOffice() {
		return office;
	}

	public void setOffice(Coordinate office) {
		this.office = office;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public LocalDateTime getFoundation() {
		return foundation;
	}

	public void setFoundation(LocalDateTime foundation) {
		this.foundation = foundation;
	}

	public Set<Plane> getPlanes() {
		return planes;
	}

	public void setPlanes(Set<Plane> planes) {
		this.planes = planes;
	}

	public Carrier(String n) {
		this.name = n;
	}
		
	public abstract LocalDateTime getNow();
	
	@Override
	public String toString() {
			return " " + "nom :" + " " + name + " " + foundation + " " + office + " f" 
			+ planes + " " + "valable le " + getNow();
	}
}
