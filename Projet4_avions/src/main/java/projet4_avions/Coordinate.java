package projet4_avions;

import java.io.Serializable;

//Serializable est indicatif et sert à informer que la classe est sérialisable.
//Ici, on sauvegarde les informations de coordonnées gps.
public class Coordinate implements Serializable{
	
	//Constante de serializable, c'est la version de la classe (ici version 1.0).
	private static final long serialVersionUID = 1L;
	private double latitude;
	private double longitude;
	private double altitude;
	
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	
	public double getAltitude() {
		return altitude;
	}
	public void setAltitude(double altitude) {
		this.altitude = altitude;
	}

	public Coordinate(double lat, double lon, double alt) {
		this.latitude = lat;
		this.longitude = lon;
		this.altitude = alt;
	}
	public Coordinate() {
		this(180, 90, 0);
	}
	
	@Override
	public String toString(){
		return "(" + latitude + "°" + ";" + " " + longitude + "°" + ";" + " " + altitude + " " + "m" + ")";
		}
	
}

