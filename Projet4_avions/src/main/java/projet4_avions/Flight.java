package projet4_avions;

import java.time.LocalDateTime;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;

public class Flight {

		private long id;
		private LocalDateTime departure;
		private String origin;
		private String destination;
		private long plane;
		
		public long getId() {
			return id;
		}
		public void setId(long id) {
			this.id = id;
		}
		public LocalDateTime getDeparture() {
			return departure;
		}
		public void setDeparture(LocalDateTime departure) {
			this.departure = departure;
		}
		public String getOrigin() {
			return origin;
		}
		public void setOrigin(String origin) {
			this.origin = origin;
		}
		public String getDestination() {
			return destination;
		}
		public void setDestination(String destination) {
			this.destination = destination;
		}
		public long getPlane() {
			return plane;
		}
		public void setPlane(long plane) {
			this.plane = plane;
		}
		
		public Flight(LocalDateTime depart, String ori, String dest, long pla) {
			this.departure = depart;
			this.origin = ori;
			this.destination = dest;
			this.plane = pla;
		}
		
		public Flight(long id, LocalDateTime depart, String ori, String dest, long pla) {
			this.id = id;
			this.departure = depart;
			this.origin = ori;
			this.destination = dest;
			this.plane = pla;
		}
		
		public String toString() {
			return "Flight:" + "  " + "id=" + id + " ; " + "departure=" + departure + " ; " + "origin=" + origin + " ; " + "destination=" + destination + " ; " + "plane=" + plane;
		}
}
