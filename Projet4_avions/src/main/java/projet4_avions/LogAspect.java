package projet4_avions;

import java.time.LocalDateTime;

import org.aspectj.lang.JoinPoint;

public class LogAspect {
	
	//Exemple de fonction associée à un pointcut.
	public void enter(JoinPoint jp) {
		System.out.println(LocalDateTime.now()+ " " + "Enter" + " " + jp.getSignature().getDeclaringTypeName());
	}
	//Exemple2 de fonction associée à un pointcut.
	public void enter2() {
		System.out.println(LocalDateTime.now()+ " " + "Leaving");
	}

}
