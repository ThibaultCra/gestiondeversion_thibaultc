package projet4_avions;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.time.LocalDateTime;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.core.io.Resource;
//javabean :
public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub, on génére le lien entre Spring(context.xml) et le java.
		System.out.println("Flotte impériale en transit");
		AbstractApplicationContext context = 
				new FileSystemXmlApplicationContext("classpath:context.xml");
		
		//Ajout de la resource ad.txt :
		Resource ad = context.getResource("classpath:ad.txt");
		try {
			File adfile = ad.getFile();
			BufferedReader r = new BufferedReader(
				new FileReader(adfile));
			
			//Obtenir le texte contenu dans ad.txt(readLine ne lit qu'une ligne):
			String l;
			while((l=r.readLine())!=null){
				System.out.println(l);
				}
			r.close();
			
		}catch(Exception ex) {
			ex.printStackTrace();	
		}
		/**
		Utilisation de Spring et des fichiers xml pour générer des objets.
		Alternative magique de nomenclature, on fait appel à notre Bean de infos.xml (Spring).**/
		
		/**
		Coordinate c1 =(Coordinate)context.getBean("towerCoord1");
		String c1Name =(String)context.getBean("towerCoord1_name");
		Object c1Size =(Object)context.getBean("sizeHangar");
		Integer c1Capa =(Integer)context.getBean("capaHangar");	
		System.out.println("c1=" + c1 + " " + c1Name + " " + c1Size + " " + c1Capa + "m2");
		
		Coordinate c2 = (Coordinate)context.getBean("towerCoord2");
		System.out.println("c2=" + c2);
		
		Storage s1 = (Storage)context.getBean("hangar1");
		System.out.println("s1=" + s1);
		
		Plane p1 = (Plane)context.getBean("plane1");
		System.out.println("p1=" + p1);
		
		LocalDateTime d1 = (LocalDateTime)context.getBean("towerCoord3");
		System.out.println("d1=" + d1);
		
		/**Notions sur le cycle de vie :
		 * Modifier p1 modifie p2 : Spring renvoie un objet sauf s'il a déjà été créé. 
		Ne fonctionne pas car plane 1 est un prototype. Les singletons sont générés à la création 
		du contexte. Lazy-init permet d'annuler cet effet de création précoce. Il n'y pas de 
		sens en prototype.**/
		/**
		p1.setAvailable(false);
		Plane p2=(Plane)context.getBean("plane1");
		p2.setModel("corvette Nidwienne");
		System.out.println("p2=" + p2);
		
		/**Le carrier implémente les fonctions d'appel de classes et de fonctions ainsi que les
		 * fonctionnalités de beans tel que mettre un bean dans un bean. ca1 fait ainsi appel dans
		 * le context.xml à :
		 * 		1- planes1,2,3 construits dans infos.xml
		 * 		2- foundation qui est un LocalDateTime.
		 * 		3- le Bean c3 ci dessous (coordonnées).
		 *  **/
		/**
		Carrier ca1 = (Carrier)context.getBean("carrier1");
		System.out.println(ca1);
		
		//c3 est ici un bean qui indique les coordonnées pour ca1.
		Coordinate c3 = (Coordinate)context.getBean("office1");
		System.out.println(c3);
		
		/**
		 * Step 1 : debut des appels pour la partie SQL du cours. Nous avons créés la classe 
		 * PlanningService. Et créés un fichier data.xml afin de définir un premier Bean appelé 
		 * ci-dessous.
		 */
		
		/**
		PlanningService pls = (PlanningService)context.getBean("planningService");
		pls.execute();
		**/
		
		BalloonService b1 = (BalloonService)context.getBean("balloonService");
		b1.execute();	
		
	}
}


