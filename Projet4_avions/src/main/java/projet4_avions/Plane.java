package projet4_avions;

public class Plane {
	
	private String model;
	private boolean available;
	
	public String getModel() {
		return model;
	}
	
	public void setModel(String model) {
		this.model = model;
	}
	
	public boolean isAvailable() {
		return available;
	}
	
	public void setAvailable(boolean available) {
		this.available = available;
	}
	
	public Plane(String mod, boolean av) {
		this.model = mod;
		this.available = av;
	}
	
	public Plane() {
		this("Banshee", true);
	}
	
	public String toString() {
		return "(" + "modele=" + model + " ; " + "disponible:" + available + ")";
	}
	
	public void init() {
		System.out.println("Decollage du batiment");
	}
	
	public void destroy() {
		System.out.println("Atterissage du batiment");
	}
}







