package projet4_avions;


import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;

public class PlanningDAO {
	
	//Fait le lien avec NamedParameterJdbcTemplate
	private NamedParameterJdbcTemplate template;
	private final static String origin = "TLS";
	
	public void setTemplate(NamedParameterJdbcTemplate template) {
		this.template = template;
	}
	
	//Méthode pour reset la jdbc après chaque compilation.
	public void init() {
		String delFlight = "DELETE FROM flight";
		template.getJdbcOperations().execute(delFlight);
		String delPlane = "DELETE FROM plane";
		template.getJdbcOperations().execute(delPlane);
	}
	
	//Méthode pour ajouter un avion à la Jdbc.
	public void insertPlaneTest(String model, String immat) {
		String insPlane = "INSERT INTO plane (model,immat) VALUES(:model,:immat)";
		Map<String, String> data = new HashMap<String,String>();
		data.put("model",model);
		data.put("immat",immat);
		template.update(insPlane, data);
	}
		
	//Version plus sophistiquée de insertPlaneTest.
	public long insertPlane(String model, String immat) {
		String insPlane = "INSERT INTO plane (model,immat) VALUES(:model,:immat)";
		MapSqlParameterSource data = new MapSqlParameterSource();
		data.addValue("model",model);
		data.addValue("immat",immat);
		GeneratedKeyHolder h = new GeneratedKeyHolder();
		template.update(insPlane, data, h);
		return h.getKey().longValue();
	}
	//Méthode pour ajouter un flight dans la Jdbc.
	public void insertFlight(Flight f) {
		String insFlight = "INSERT INTO flight(departure,origin,destination,plane) VALUES(:departure,:origin,:destination,:plane)";
		MapSqlParameterSource dataF = new MapSqlParameterSource();
		dataF.addValue("departure", f.getDeparture());
		dataF.addValue("origin", f.getOrigin());
		dataF.addValue("destination", f.getDestination());
		dataF.addValue("plane", f.getPlane());
		template.update(insFlight, dataF);
	}
	//Méthode sophistiquée pour sélectionner un flight à partir de son origine.
	public List<Flight> getFlightByOrigin(String origin) {
		String selFlight="SELECT * FROM flight WHERE origin LIKE:origin";
		MapSqlParameterSource data = new MapSqlParameterSource();
		data.addValue("origin", origin);
		
		//On crée une deuxième liste res (Flight) puis on va chercher la Map (m) dans la res.
		List<Flight> res = new ArrayList<Flight>();
		for(Map<String,Object> m:template.queryForList(selFlight, data)) {
			res.add(new Flight((Integer)m.get("id"),((Timestamp) m.get("departure")).toLocalDateTime(), 
			(String) m.get("origin"), (String) m.get("destination"), (Integer)m.get("plane")));
		}return res;
	}
	//Une lecture et 2 écritures. Ajoute un voyage : aller et retour (toulouse).
	public void addJourney(String immat, String destination, LocalDateTime dep1, LocalDateTime dep2) {
		
		String selPlane = "SELECT id FROM plane WHERE immat=:immat";
		MapSqlParameterSource data = new MapSqlParameterSource();
		data.addValue("immat", immat);
		long id =(long)template.queryForObject(selPlane, data, Long.class);
			
		String airport = destination;
		
		String allerFlight = "INSERT INTO flight(departure,origin,destination,plane) VALUES(:departure,:origin,:destination,:plane)";
		MapSqlParameterSource dataAller = new MapSqlParameterSource();
		dataAller.addValue("departure", dep1);
		dataAller.addValue("origin", origin);
		dataAller.addValue("destination", airport);
		dataAller.addValue("plane", id);
		template.update(allerFlight, dataAller);
		
		String retourFlight = "INSERT INTO flight(departure,origin,destination,plane) VALUES(:departure,:origin,:destination,:plane)";
		MapSqlParameterSource dataRetour = new MapSqlParameterSource();
		dataRetour.addValue("departure", dep2);
		dataRetour.addValue("origin", airport);
		dataRetour.addValue("destination", origin);
		dataRetour.addValue("plane", id);
		template.update(retourFlight, dataRetour);
	}
}

//La suite de la chaîne se passe dans data.xml : liaison à DriverManagerDataSource puis à la bdd
