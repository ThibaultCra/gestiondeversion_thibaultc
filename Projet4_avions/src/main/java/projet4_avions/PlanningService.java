package projet4_avions;

import java.time.LocalDateTime;

public class PlanningService {
	
	private PlanningDAO dao;

	public void execute() {
		//System.out.println("Planning : ");
		long p1=dao.insertPlane("hyperion", "avionabc");
		long p2=dao.insertPlane("sanctus", "aviondef");
		long p3=dao.insertPlane("himmel", "avionghi");
		dao.insertFlight(new Flight(LocalDateTime.of(1950,2,10,10,10), "TLS", "MRL", p1));
		dao.insertFlight(new Flight(LocalDateTime.of(1960,2,10,10,10), "NIC", "TLS", p2));
		dao.insertFlight(new Flight(LocalDateTime.of(1970,2,10,10,10), "TLS", "CUB", p3));
		System.out.println(dao.getFlightByOrigin("TLS"));
		dao.addJourney("avionabc", "MEX", LocalDateTime.of(1950,2,10,10,10), LocalDateTime.of(1960,2,10,10,10));
	}
	//Fait le lien avec le DAO	
	public void setdao(PlanningDAO dao) {
		this.dao = dao;
	}
	
	/**
	 * Spring dispose de ressources pour organiser l'accés JDBC. Spring peut nous
	 * aider à utiliser Hibernate : avec HibernateTemplate. Spring déconseille cette
	 * solution et conseille un accés direct à Hibernate. Ici nous ferons avec la
	 * JDBC Template de Spring. DriverManagerDataSource : établit la connection et
	 * l'authentification. NamedParameterJdbcTemplate : gère les données en
	 * interface avec le DAO.
	 */

}
