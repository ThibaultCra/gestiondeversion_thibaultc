package projet4_avions;

import java.util.ArrayList;
import java.util.List;

public class Storage {

	private List<Double> list;
	
	public int getNumber() {
		return list.size();
	}
	
	public List<Double> getList() {
		return list;
	}
	
	public void setList(List<Double> list) {
		this.list = list;
	}	
	
	public Storage() {
		this.list = new ArrayList<Double>();	
	}
	
	public void addVolume(double v) {
		list.add(v);
	}
		
	public String toString() {
		return list + " ";
	}
}
