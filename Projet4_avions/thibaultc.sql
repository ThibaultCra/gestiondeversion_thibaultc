-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  jeu. 04 avr. 2019 à 08:21
-- Version du serveur :  5.7.24
-- Version de PHP :  7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `thibaultc`
--

-- --------------------------------------------------------

--
-- Structure de la table `flight`
--

DROP TABLE IF EXISTS `flight`;
CREATE TABLE IF NOT EXISTS `flight` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `departure` datetime NOT NULL,
  `origin` char(3) NOT NULL,
  `destination` char(3) NOT NULL,
  `plane` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `plane` (`plane`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=latin1 COMMENT='Table Flight.';

--
-- Déchargement des données de la table `flight`
--

INSERT INTO `flight` (`id`, `departure`, `origin`, `destination`, `plane`) VALUES
(49, '1950-02-10 09:10:00', 'TLS', 'MRL', 85),
(50, '1960-02-10 09:10:00', 'NIC', 'SPT', 86),
(51, '1970-02-10 09:10:00', 'PAR', 'CUB', 87),
(52, '1950-02-10 09:10:00', 'TLS', 'MEX', 85),
(53, '1960-02-10 09:10:00', 'MEX', 'TLS', 85);

-- --------------------------------------------------------

--
-- Structure de la table `plane`
--

DROP TABLE IF EXISTS `plane`;
CREATE TABLE IF NOT EXISTS `plane` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model` varchar(15) NOT NULL,
  `immat` char(8) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `immat` (`immat`)
) ENGINE=MyISAM AUTO_INCREMENT=88 DEFAULT CHARSET=latin1 COMMENT='Table Plane des avions';

--
-- Déchargement des données de la table `plane`
--

INSERT INTO `plane` (`id`, `model`, `immat`) VALUES
(85, 'hyperion', 'avionabc'),
(86, 'sanctus', 'aviondef'),
(87, 'himmel', 'avionghi');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
