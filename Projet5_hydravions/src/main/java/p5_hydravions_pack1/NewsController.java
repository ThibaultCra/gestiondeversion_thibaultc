package p5_hydravions_pack1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class NewsController {

	private final Logger log = LoggerFactory.getLogger(NewsController.class);
	@Value("${news.title}")
	private String newsTitle;
	@Value("${news.text}")
	private String newsText;

	@RequestMapping("/news")
	public String show(Model model) {
		model.addAttribute("title", newsTitle);
		model.addAttribute("text", newsText);
		log.info("show news" + " " + newsTitle + " " + newsText);
		return "news";
	}
	
	
}
