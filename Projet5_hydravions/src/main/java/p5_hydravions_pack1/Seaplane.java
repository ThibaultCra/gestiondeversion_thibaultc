package p5_hydravions_pack1;

public class Seaplane {

	private boolean outshore;
	private String model;
	private String registration;
	
	public boolean isOutshore() {
		return outshore;
	}
	public void setOutshore(boolean outshore) {
		this.outshore = outshore;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getRegistration() {
		return registration;
	}
	public void setRegistration(String registration) {
		this.registration = registration;
	}
	
	public Seaplane(boolean outshore, String model, String registration) {
		this.outshore = outshore;
		this.model = model;
		this.registration = registration;
	}
	
	public String toString() {
		return "outshore=" + outshore + " " + "model=" + model + " " + "registration=" + registration;
	}
}
