package p5_hydravions_pack1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class SeaplaneController {

	private SeaplaneSource seaplaneS;
	
	public SeaplaneSource getSeaplaneS() {
		return seaplaneS;
	}
	@Autowired
	public void setSeaplaneS(SeaplaneSource seaplaneS) {
		this.seaplaneS = seaplaneS;
	}

	@RequestMapping("/seaplanes.html")
	public String show(Model m) {
		m.addAttribute("l", seaplaneS.getList());
		return "seaplanes";
	}
	
	
}