package p5_hydravions_pack1;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class SeaplaneSource {
	
	public List<Seaplane> getList() {
		ArrayList<Seaplane> hydravion = new ArrayList<Seaplane>();
		hydravion.add(new Seaplane(false, "FatSeaplane","EAGJGUA"));
		hydravion.add(new Seaplane(true, "StarDestroyer","IFHZFGZ"));
		hydravion.add(new Seaplane(false, "HugeWhale","OGROGRG"));
		hydravion.add(new Seaplane(true, "Landrover","AGGAGYA"));
		return hydravion;
	}

}
