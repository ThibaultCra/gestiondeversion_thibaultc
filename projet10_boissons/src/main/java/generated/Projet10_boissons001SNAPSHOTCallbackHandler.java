/**
 * Projet10_boissons001SNAPSHOTCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.9  Built on : Nov 16, 2018 (12:05:37 GMT)
 */
package generated;


/**
 *  Projet10_boissons001SNAPSHOTCallbackHandler Callback class, Users can extend this class and implement
 *  their own receiveResult and receiveError methods.
 */
public abstract class Projet10_boissons001SNAPSHOTCallbackHandler {
    protected Object clientData;

    /**
     * User can pass in any object that needs to be accessed once the NonBlocking
     * Web service call is finished and appropriate method of this CallBack is called.
     * @param clientData Object mechanism by which the user can pass in user data
     * that will be avilable at the time this callback is called.
     */
    public Projet10_boissons001SNAPSHOTCallbackHandler(Object clientData) {
        this.clientData = clientData;
    }

    /**
     * Please use this constructor if you don't want to set any clientData
     */
    public Projet10_boissons001SNAPSHOTCallbackHandler() {
        this.clientData = null;
    }

    /**
     * Get the client data
     */
    public Object getClientData() {
        return clientData;
    }

    /**
     * auto generated Axis2 call back method for getAdviceEnum method
     * override this method for handling normal response from getAdviceEnum operation
     */
    public void receiveResultgetAdviceEnum(
        generated.Projet10_boissons001SNAPSHOTStub.GetAdviceEnumResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from getAdviceEnum operation
     */
    public void receiveErrorgetAdviceEnum(java.lang.Exception e) {
    }

    // No methods generated for meps other than in-out

    /**
     * auto generated Axis2 call back method for getLastAdvices method
     * override this method for handling normal response from getLastAdvices operation
     */
    public void receiveResultgetLastAdvices(
        generated.Projet10_boissons001SNAPSHOTStub.GetLastAdvicesResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from getLastAdvices operation
     */
    public void receiveErrorgetLastAdvices(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for forgetAdvices method
     * override this method for handling normal response from forgetAdvices operation
     */
    public void receiveResultforgetAdvices() {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from forgetAdvices operation
     */
    public void receiveErrorforgetAdvices(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for getHealth method
     * override this method for handling normal response from getHealth operation
     */
    public void receiveResultgetHealth(
        generated.Projet10_boissons001SNAPSHOTStub.GetHealthResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from getHealth operation
     */
    public void receiveErrorgetHealth(java.lang.Exception e) {
    }
}
