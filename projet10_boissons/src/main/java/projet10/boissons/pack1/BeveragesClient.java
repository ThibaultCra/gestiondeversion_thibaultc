package projet10.boissons.pack1;

import generated.Projet10_boissons001SNAPSHOTStub;
import generated.Projet10_boissons001SNAPSHOTStub.Food;
import generated.Projet10_boissons001SNAPSHOTStub.GetAdviceEnum;
import generated.Projet10_boissons001SNAPSHOTStub.GetAdviceEnumResponse;
import generated.Projet10_boissons001SNAPSHOTStub.GetHealth;
import generated.Projet10_boissons001SNAPSHOTStub.GetHealthResponse;

public class BeveragesClient {

	public static void main(String[] args) throws Exception {
		
		Projet10_boissons001SNAPSHOTStub stub = new Projet10_boissons001SNAPSHOTStub();
		
		GetHealth getHealth = new GetHealth();
		GetHealthResponse getHealthResponse = stub.getHealth(getHealth);
		System.out.println(getHealthResponse.get_return());
		
		GetAdviceEnum getAdviceEnum = new GetAdviceEnum();
		getAdviceEnum.setFood(Food.MEAT);
		GetAdviceEnumResponse getAdviceEnumResponseMeat = stub.getAdviceEnum(getAdviceEnum);
		System.out.println(getAdviceEnumResponseMeat.get_return());
		
		GetAdviceEnum getAdviceEnum2 = new GetAdviceEnum();
		getAdviceEnum2.setFood(Food.FISH);
		GetAdviceEnumResponse getAdviceEnumResponseFish = stub.getAdviceEnum(getAdviceEnum2);
		System.out.println(getAdviceEnumResponseFish.get_return());
		
	}

}
