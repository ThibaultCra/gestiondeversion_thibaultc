package projet10.boissons.pack1;

import java.util.ArrayList;
import java.util.List;

public class BeveragesService {
	
	private String beverageMeat = "red whine";
	private String beverageFish = "white whine";
	private String beverageDefault = "water";
	private List<String> lastAdvices = new ArrayList<>();
	public enum Food {MEAT, FISH};
	
	public String getHealth() {
		return "OK";
	}

	public String getAdviceEnum(Food food) {
		if(lastAdvices.size()>3) {
			lastAdvices.remove(0);
		}			
		switch(food) {
		case MEAT : lastAdvices.add(beverageMeat); return beverageMeat;
		case FISH : lastAdvices.add(beverageFish); return beverageFish;
		}
		lastAdvices.add(beverageDefault);
		return beverageDefault;
	}
	
	public void forgetAdvices() {
		lastAdvices.clear();
	}
	
	public void setLastAdvices(List<String> lastAdvices) {
		this.lastAdvices = lastAdvices;
	}

	public List<String> getLastAdvices() {
		return lastAdvices;
	}
	
	//Poubelle à fonctions :
	/**
	public String getAdvice(String food) {
		if ((food != null && food.contentEquals("meet"))) {
			return beverageMeat;
		} else if ((food != null && food.contentEquals("fish"))) {
			return beverageFish;
		}
		return beverageDefault;
	}**/
}







