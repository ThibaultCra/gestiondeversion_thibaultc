package projet10.boissons.tests;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import projet10.boissons.pack1.BeveragesService;
import projet10.boissons.pack1.BeveragesService.Food;

class BeveragesTest {

	@Test
	public void test() {
		BeveragesService a = new BeveragesService();
		Assert.assertEquals(a.getAdviceEnum(Food.MEAT), "red whine");
	}

}
