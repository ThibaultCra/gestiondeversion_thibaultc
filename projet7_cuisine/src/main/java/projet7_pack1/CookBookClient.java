package projet7_pack1;

import java.util.LinkedHashMap;

import org.springframework.web.client.RestTemplate;

public class CookBookClient {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			RestTemplate rt = new RestTemplate();
			
			Boolean complete = rt.getForObject("http://localhost:8083/complete", Boolean.class);
			System.out.println("Complet ? " + complete);
			
			Receipe tarte = rt.getForObject("http://localhost:8083/receipes/0", Receipe.class);
			System.out.println("Recette = " + tarte.getName());
			
			System.out.println("WP = " + rt.getForObject
					("https://fr.wikipedia.org/w/api.php?action=query&list=search&format=json&srsearch=tartelette", 
					LinkedHashMap.class));
			
			rt.delete("http://localhost:8083/receipes/0", Receipe.class);
			
			rt.postForLocation("http://localhost:8083/receipes", new Receipe("Gateau", 20, "Cuit au feu de bois"));
					
		} catch(Exception ex) {
			ex.printStackTrace();
		}
	}

	
}
