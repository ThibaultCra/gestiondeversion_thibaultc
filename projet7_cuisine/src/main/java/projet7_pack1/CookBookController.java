package projet7_pack1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

//Type de contrôleur spécialisé des requêtes REST.
@RestController//@RequestMapping("/v1") => Pour imposer un appel pour toutes les méthodes.
public class CookBookController {
	
	//Voir index.html pour l'affichage.
	@RequestMapping(value ="/complete", method=RequestMethod.GET, produces="application/json")
	public boolean isComplete() {
		return false;
	}
	
	//Voir index.html pour l'affichage.
	@GetMapping(value="/health", produces="application/json")//Autre syntaxe pour RequestMapping (value="x", method=RequestMethod.GET)
	public Map<String, String> getHealth() {
		Map<String, String> hm = new HashMap<String, String>();
		hm.put("CookBook", "incomplete");
		hm.put("data", "OK");
		hm.put("server", "OK");
		return hm;
	}
	
	//Visualisation en JSON d'une liste de recettes.
	@GetMapping(value ="/receipes", produces="application/json")
	public List<Receipe> getReceipes(){
		List<Receipe> receipeList = new ArrayList<Receipe>();
		receipeList.add(new Receipe("Tarte", 60, "Cuisson au four"));
		receipeList.add(new Receipe("Cookies", 10, "Cuisson courte"));
		receipeList.add(new Receipe("Banofee", 120, "Refrigération"));
		receipeList.add(new Receipe("Cannelés", 140, "Moules en silicone"));
		receipeList.add(new Receipe("Brownie", 40, "Cuisson au four"));
		receipeList.add(new Receipe("Boules coco", 200, "Refrigération"));
		return receipeList;
	}
	
	//Une façon de visualiser complete en XML (/!\Une dépendance Maven est nécessaire/!\).
	@GetMapping(value="/complete.xml", produces="text/xml")
	public boolean isCompleteXML() {
		return isComplete();
	}
	
	//Fonction élégante pour afficher une recette en fonction de son numero (avec PathVariable).
	@GetMapping(produces="application/json", value="/receipes/{n}")//Appelle le numero de la recette.
	public Receipe getReceipe(@PathVariable("n") int n) {
		return getReceipes().get(n);
	}
	
	@DeleteMapping(value="/receipes/{n}")
	public void deleteReceipe(@PathVariable("n") int n) {
		System.out.println("Recette supprimée : " + n);
	}
	
	@PostMapping(value="/receipes")
	public void postReceipe(@RequestBody Receipe r) {
		System.out.println("Recette postée : " + r.getName());
	}
}


