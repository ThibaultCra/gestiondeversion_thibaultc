package projet7_pack1;

public class Receipe {
	
	private String name;
	private int duration;
	private String text;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getDuration() {
		return duration;
	}
	public void setDuration(int duration) {
		this.duration = duration;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Receipe(String name, int duration, String text) {
		this.name = name;
		this.duration = duration;
		this.text = text;
	}
	public Receipe() {
		this("tarte", 0, "faire une tarte");
	}	
	@Override
	public String toString() {
		return "Receipe [name=" + name + ", duration=" + duration + ", text=" + text + "]";
	}

}
