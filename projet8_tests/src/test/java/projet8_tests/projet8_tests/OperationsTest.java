package projet8_tests.projet8_tests;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class OperationsTest {
	
	private static long l1 = System.nanoTime();
	private static long l2 = System.nanoTime();
	private static long l3 = l1 + l2;

	//Version Hamcrest :
	@Test
	public void additionAvecDeuxNombres1() {
		assertThat(Operations.additionner(6, 5), is(11L)); //Il faut lui indiquer que le 11 est un Long
	}
	
	@Test
	public void additionRecursive() {
		Assert.assertEquals(11, Operations.additionner(6, 5));
	}
	
	@Test
	public void additionAvecCinqNombres() {
		Assert.assertEquals(20, Operations.additionner(2, 3, 4, 5, 6));
	}
	
	@Test
	public void additionAvecDeuxNombres2() {
		final long lAddition1 = Operations.additionner(10, 20);
		final long lAddition2 = Operations.additionner(14,16);
		Assert.assertSame(lAddition1, lAddition2);
	}

	@Test
	public void mutiplicationAvecDeuxNombres() {
		Assert.assertEquals(12, Operations.multiplier1(4, 3));
	}
	
	@Test
	public void mutiplicationAvecCinqNombres1() {
		Assert.assertEquals(720, Operations.multiplier2(2, 3, 4, 5, 6));
	}
	
	@Test
	public void multiplicationParametre() {
		TestParametre.getParametres();
	}
	
	@Test(expected=Exception.class)
	public void xDivisionAvecUnNombre() {
		Operations.diviser(2);
	}
	
	@Test(expected=ArithmeticException.class)
    public void xDivisionAvecDeuxNombresDontUnZero() {
        Operations.diviser(10, 0);
    }
	
	@Test
	public void xIsRoughlySameLong() {
		Long lLong1 = new Long(1);
		Long lLong2 = new Long(1);
	Assert.assertEquals(lLong1, lLong2);
	}
	
	@Test
	public void xIsSameLong() {
		Long lLong1 = new Long(1);
		Long lLong2 = lLong1;
	Assert.assertSame(lLong1, lLong2);
	}
	
	/** Test qui échoue à s'effectuer en 1 seconde :
	
	@Test(timeout=1000)
	public void testMutiplicationAvecCinqNombres2() {
		final long lMultiplication = Operations.multiplier2(256, 512, 1024, 2048, 4096);
		Assert.assertEquals(1125899906842624L, lMultiplication);
	}
	**/
	
	@Before
	public void calcTempsEntree() {
		System.out.println(l1);
	}
	
	@After
	public void calcTempsSortie() {
		System.out.println(l2);
	}
	
	@AfterClass
	public static void calcTempsTot() {
		System.out.println(l3);
	}
}
