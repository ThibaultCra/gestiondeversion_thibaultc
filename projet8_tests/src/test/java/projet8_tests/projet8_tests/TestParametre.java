package projet8_tests.projet8_tests;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class TestParametre {

	long mAttendu;
	long[] mParametre;

	@Parameters
	public static List<Object[]> getParametres() {
		return Arrays.asList(new Object[][] { { 200L, new long[] { 10L, 20L } }, { 300L, new long[] { 10L, 30L } },
				{ 200L, new long[] { 10L, 20L } }, });

	}

	public TestParametre(long pAttendu, long[] pParametre) {
		mAttendu = pAttendu;
		mParametre = pParametre;
	}

}
