package projet9_pack1;

import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;

public class ClientMain {

	public static void main(String[] args) {
		JaxWsProxyFactoryBean jax = new JaxWsProxyFactoryBean();
		jax.setAddress("http://localhost:8082/patisserie");
		jax.setServiceClass(PatisserieInterface.class);
		jax.getOutInterceptors().add(new LoggingOutInterceptor());
		jax.getInInterceptors().add(new LoggingInInterceptor());
		PatisserieInterface p = (PatisserieInterface) jax.create();
		System.out.println(p.isAvailable("cherry"));
		System.out.println(p.isAvailable("papaye"));
		System.out.println(p.isAvailable("ananas"));
		System.out.println(p.isAvailable("fraise"));
		for (Ustensil u : p.getUstensilListe()) {
			System.out.println("Ustensil : " + "Nom = " + u.getName() + "; Taille = " + u.getSize() + "; Image = "
					+ u.getImg().getContentType());
		}
	}

}
