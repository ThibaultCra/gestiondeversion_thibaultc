package projet9_pack1;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.jws.WebService;

@WebService
public class Patisserie implements PatisserieInterface {

	private String error = null;
	private List<Ustensil> outils = new ArrayList<>();
	
	@Override
	public String getError() {
		return error;
	}

	@Override
	public boolean isAvailable(String what, String pwd) {
		if (!pwd.equals("1234"))

		{
			throw new SecurityException("Mauvais mot de passe");
		}
		return isAvailable(what);
	}

	@Override
	public boolean isAvailable(String what) {

		if (what == null || what.isEmpty()) {
			error = "empty";
			throw new IllegalArgumentException("empty");
		}

		int m = LocalDate.now().getMonthValue();
		if ((what.equals("cherry") && (m < 5 || m > 7)) || (what.equals("ananas") && (m < 1 || m > 3))
				|| (what.equals("papaye") && (m < 10 || m > 12)) || (what.equals("fraise") && (m < 1 || m > 12))) {
			return false;
		}

		return true;
	}

	@Override
	public List<Ustensil> getUstensilListe() {
		outils.add(new Ustensil("cuiller", 10, new DataHandler(new FileDataSource("img/img1.jpg"))));
		outils.add(new Ustensil("moule à gauffre", 20, new DataHandler(new FileDataSource("img/img2.jpg"))));
		outils.add(new Ustensil("plat à tarte", 30, new DataHandler(new FileDataSource("img/img3.jpg"))));
		outils.add(new Ustensil("epluche legume", 5, new DataHandler(new FileDataSource("img/img4.jpg"))));
		return outils;
	}
	
	public String toString(String outils) {
		return outils;
		
	}

	@Override
	public Ustensil[] getUstensilTable() {
		return new Ustensil[] { 
				new Ustensil("cuiller", 10, new DataHandler(new FileDataSource("img/img1.jpg"))),
				new Ustensil("moule à gauffre", 20, new DataHandler(new FileDataSource("img/img2.jpg"))),
				new Ustensil("plat à tarte", 30, new DataHandler(new FileDataSource("img/img3.jpg"))),
				new Ustensil("epluche legume", 5, new DataHandler(new FileDataSource("img/img4.jpg"))), 
				};
	}
}
