package projet9_pack1;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface PatisserieInterface {

	public String getError();
	public boolean isAvailable(String what);
	@WebMethod(operationName="isAvailablePassword")
	public boolean isAvailable(String what, @WebParam(header=true, name="pwd") String pwd);
	public List<Ustensil> getUstensilListe();
	public Ustensil[] getUstensilTable();
}
