package projet9_pack1;

import javax.activation.DataHandler;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

@XmlType(name="Ustensil")
public class Ustensil {

	private String name;
	private int size;
	private DataHandler img;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	//@XmlTransient//Enlève cet attribut du XML obtenu
	public int getSize() {
		return size;
	}
	public Ustensil() {
		super();
	}
	public void setSize(int size) {
		this.size = size;
	}
	public DataHandler getImg() {
		return img;
	}
	public void setImg(DataHandler img) {
		this.img = img;
	}
	public Ustensil(String name, int size, DataHandler img) {
		this.name = name;
		this.size = size;
		this.img = img;
	}
	
}
