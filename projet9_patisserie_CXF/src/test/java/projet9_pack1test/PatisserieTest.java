package projet9_pack1test;

import static org.junit.Assert.*;

import org.junit.Test;

import projet9_pack1.Patisserie;

public class PatisserieTest {

	@Test
	public void testGetError() {
		Patisserie p = new Patisserie();
		assertEquals(p.getError(), null);
	}
	
	@Test
	public void testIsAvailable1() {
		Patisserie m = new Patisserie();
		assertEquals(m.isAvailable("cherry"), true);
	}
	
	@Test
	public void testIsAvailable2() {
		Patisserie m = new Patisserie();
		assertEquals(m.isAvailable("ananas"), false);
	}
	
	@Test
	public void testIsAvailable3() {
		Patisserie m = new Patisserie();
		assertEquals(m.isAvailable("papaye"), false);
	}

	@Test
	public void testIsAvailable4() {
		Patisserie m = new Patisserie();
		assertEquals(m.isAvailable("fraise"), true);
	}
}
